import express, { Express } from "express";
import cors from "cors";
import winston from "winston";
import bodyParser from "body-parser";
import expressWinston from "express-winston";

const expreesConfig = (app: Express): Express => {
  app.use(bodyParser.json());
  app.use(cors());
  app.options("*", cors());
  app.use(
    bodyParser.urlencoded({
      extended: true,
    })
  );
  app.use("/static", express.static("uploads"));
  app.use(
    expressWinston.logger({
      transports: [new winston.transports.Console()],
      format: winston.format.combine(winston.format.json()),
      meta: false, // optional: control whether you want to log the meta data about the request (default to true)
      msg: "HTTP {{req.method}} {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
      expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
      colorize: false, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
      ignoreRoute(req: any, res: any) {
        return false;
      }, // optional: allows to skip some log messages based on request and/or response
    })
  );

  return app;
};

export default expreesConfig;
