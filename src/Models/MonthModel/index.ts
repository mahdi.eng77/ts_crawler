import mongoose, { ObjectId } from "mongoose";
const { Schema, model } = mongoose;
export interface DayFiled {
  url?: string;
  "Wind NW"?: string;
  "Wind Gusts"?: string;
  "Cloud Cover"?: string;
  Precipitation?: string;
  "Probability of Thunderstorms"?: string;
  "Probability of Precipitation"?: string;
  Wind?: string;
}
export interface IMonth {
  _id: ObjectId;
  page_url: string;
  exTime: Date;
  day_list: DayFiled[];
}

// 2. Create a Schema corresponding to the document interface.
const MonthSchema = new Schema<IMonth>({
  page_url: { type: String, required: true },
  exTime: { type: Date, default: Date.now() },
});

export default model<IMonth>("Month", MonthSchema);
