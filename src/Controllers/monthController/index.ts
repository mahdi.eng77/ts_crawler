import * as models from "../../Models";
import { fetchHtmlFromUrl } from "../../Utils";
import { Request, Response } from "express";
import fs from "fs";
const urlSuffix = "https://www.accuweather.com";
// const monthUrlList = urlSuffix + "/en/gb/london/ec4a-2/october-weather/328328";
const startGethingMonthData = async (req: Request, res: Response) => {
  try {
    const { url } = req.body;

    const requestHtml = await fetchHtmlFromUrl(url);
    const dayObjectList = requestHtml(".monthly-daypanel");
    const urlList: string[] = [];
    dayObjectList.each((i: any, e: any) => {
      const { href } = e.attribs;
      if (href) urlList.push(href);
    });

    /* -------------------------------------------------------------------------- */
    /*          create list of request and hold it for send at same time          */
    const dayReqHtmlList = urlList.map(async (dayUrl) => {
      return await fetchHtmlFromUrl(urlSuffix + dayUrl);
    });
    /* -------------------------------------------------------------------------- */

    /* -------------------------------------------------------------------------- */
    /*              send all request at once and return list of data              */
    Promise.all(dayReqHtmlList)
      .then((data) => {
        const allDaya = data.map((node, index) =>
          getDayData(node, urlList[index])
        );
        const monthRaw = {
          page_url: url,
          day_list: allDaya,
        };
        generateJsonFile(monthRaw, url);
        res.json(monthRaw);
      })
      .catch((e) => {
        console.log(e);
      });
    /* -------------------------------------------------------------------------- */

    // const MonthBody = new models.Month(monthRaw);

    /* ----------------- now i can save data of the page into db ---------------- */

    // MonthBody.save((err, saveRes: models.IMonth) => {
    //   if (err) return res.status(400).send(err);
    // });
  } catch (err) {
    res.send(err);
  }
};

const getDayData = (data: any, url: string) => {
  const tempList = data(".value");
  const result: models.DayFiled = {};
  tempList.each((i: any, e: any) => {
    // @ts-ignore
    result[e.prev.data] = e.children[0].data;
  });
  result.url = url;
  return result;
};

const generateJsonFile = (data: any, fileName: string) => {
  const jsonString = JSON.stringify(data);
  fs.writeFile(`./savedJsonFile/${Date.now()}.json`, jsonString, (err: any) => {
    if (err) {
      console.log("Error writing file", err);
    } else {
      console.log("Successfully wrote file");
    }
  });
};

export { startGethingMonthData };
