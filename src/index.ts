import express from "express";
import versinoRouter from "./Routes";
import { expreesConfig } from "./Config";
const app = express();
const application = expreesConfig(app);
const port = 9000; // default port to listen
application.use("/api", versinoRouter);
// define a route handler for the default home page
application.get("/", (req, res) => {
  res.send("Hello world!");
});

// start the Express server
application.listen(port, () => {
  console.log(`server started at http://localhost:${port}`);
});
