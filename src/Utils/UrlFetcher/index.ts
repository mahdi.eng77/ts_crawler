import axios from "axios";
import cheerio from "cheerio";
export const fetchHtmlFromUrl = async (url: string) => {
  return await axios
    .get(url)
    .then((response) =>
      cheerio.load(response.data, {
        xmlMode: true,
        decodeEntities: true, // Decode HTML entities.
        withStartIndices: false, // Add a `startIndex` property to nodes.
        withEndIndices: false, // Add an `endIndex` property to nodes.
      })
    )
    .catch((error) => {
      error.status = (error.response && error.response.status) || 500;
      throw error;
    });
};
