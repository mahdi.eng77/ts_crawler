import express from "express";

import monthRouter from "./MonthRouter";

const combineRouter = express.Router();

combineRouter.use("/month", monthRouter);

export default combineRouter;
