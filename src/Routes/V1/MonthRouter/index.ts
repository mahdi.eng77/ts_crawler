import express from "express";
import { monthController } from "../../../Controllers";
const monthRouter = express.Router();

monthRouter.post("/", monthController.startGethingMonthData);

export default monthRouter;
