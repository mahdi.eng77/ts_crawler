import express from "express";
import routerV1 from "./V1";
const versinoRouter = express.Router();

versinoRouter.use("/v1", routerV1);

export default versinoRouter;
